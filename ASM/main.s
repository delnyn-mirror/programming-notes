section .data ; constants (a section is a code segment)
msg dw 'Hello from Assembly!', 0xa  ;string to be printed
len equ $ - msg     ;length of the string

section .text ; code
	global _start ; tell the kernel what to execute

_start:
   mov	rdx,len     ;message length                 }
   mov	rcx,msg     ;message to write               }
   mov	rbx,1       ;file descriptor (stdout)       } THIS IS HOW YOU DO A SYSTEM CALL (REMEMBER THE BOOKS)
   mov	rax,4       ;system call number (sys_write) }
   int	0x80        ;call kernel                    }
	
   mov	rax,1       ;system call number (sys_exit)
   int	0x80        ;call kernel
